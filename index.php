<?php

require('animal.php');
require('ape.php');
require('frog.php');

$sheep = new Animal("shaun");

echo "Nama Hewan : $sheep->name <br>"; // "shaun"
echo "jumlah kaki : $sheep->legs <br>"; // 4
echo "tipe darah : $sheep->cold_blooded <br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$ape = new ape("sun gu kong");

echo "Nama Hewan : $ape->name <br>"; // "shaun"
echo "jumlah kaki : $ape->legs <br>"; // 4
echo "tipe darah : $ape->cold_blooded <br>"; // "no"
$ape->yell();
echo"<br>";

$frog = new frog("buduk");

echo "Nama Hewan : $frog->name <br>"; // "shaun"
echo "jumlah kaki : $frog->legs <br>"; // 4
echo "tipe darah : $frog->cold_blooded <br>"; // "no"
$frog->jump();